# Application Self-Update

The task is to make a proposal for a self-update procedure and implement it for
simple application. The simple application in this case is an HTTP server which is able to
update itself.

To run the experiment, first build `ver2.go` and call the binary `ver2`, then run `server.go` and at last run `app/main.go` before opening `http://localhost:8080/` in a browser.

## Security concerns
The new version is downloaded via HTTP allowing a man-in-the-middle to replaces the executable. The server has no time-out or any other DoS prevention measures.

The application was tested and developed on Ubuntu using the Firefox browser