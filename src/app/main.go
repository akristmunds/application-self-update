package main

import (
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"syscall"
)

const (
	currentVersion = "ver1"
	mainTemplate   = `<!DOCTYPE html>
<html lang="en">
	<head>
		<title> Server {{.CurrentVersion}} </title>
	</head>
	<body>
		<h1>This server has {{.CurrentVersion}}</h1>
		<a href="check">Check version</a>
		<br>
		{{if .ButtonWasClickedOn}}
			{{if eq .CurrentVersion .LatestVersion}}
				You are running the latest version
			{{else}}
				New version is available: {{.LatestVersion}} | <a href="install">Download and install</a>
			{{end}}
		{{end}}
	</body>
</html>
`
	afterInstallation = `<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Installation Successful!</title>
	</head>
	<body>
		<h1>Installation Successful!</h1>
		<a href="/">Go to new version</a>
	</body>
</html>
`
)

type page struct {
	t *template.Template
}

func newPage(templ string) page {
	t, err := template.New("page").Parse(templ)
	logIfPresent(err)
	return page{t: t}
}

func (p page) execute(w http.ResponseWriter, data interface{}) {
	if err := p.t.Execute(w, data); err != nil {
		log.Fatal(err)
	}
}

func (p page) writeStatus(w http.ResponseWriter, latestVersion string, buttonWasClickedOn bool) {
	p.execute(w, struct {
		CurrentVersion, LatestVersion string
		ButtonWasClickedOn            bool
	}{currentVersion, latestVersion, buttonWasClickedOn})
}

func main() {

	p := newPage(mainTemplate)

	// main p> show version and Update check
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		p.writeStatus(w, currentVersion, false)
	})

	// check for updates
	http.HandleFunc("/check", func(w http.ResponseWriter, r *http.Request) {
		p.writeStatus(w, getLatestVersion(), true)
	})

	// download and update
	http.HandleFunc("/install", func(w http.ResponseWriter, r *http.Request) {
		newVersion := getLatestVersion()
		responseBody := getResponseBody("http://localhost:8081/download")
		defer safelyClose(responseBody)
		log.Printf("Create file %s", newVersion)
		file, osErr := os.Create(newVersion)
		logIfPresent(osErr)
		defer safelyClose(file)
		log.Printf("Copy request body into file %s", newVersion)
		if _, err := io.Copy(file, responseBody); err != nil {
			log.Fatal(err)
		}
		if err := os.Chmod(newVersion, 0775); err != nil {
			log.Fatal(err)
		}
		newPage(afterInstallation).execute(w, struct{}{})
		go func() {
			log.Print("Switching processes...")
			if execErr := syscall.Exec(newVersion, []string{newVersion}, os.Environ()); execErr != nil {
				log.Fatal(execErr)
			}
		}()
	})

	// serve
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func getLatestVersion() string {
	responseBody := getResponseBody("http://localhost:8081/latestVersion")
	defer safelyClose(responseBody)
	body, ioUtilErr := ioutil.ReadAll(responseBody)
	logIfPresent(ioUtilErr)
	return string(body)
}

func getResponseBody(url string) io.ReadCloser {
	response, err := http.Get(url)
	logIfPresent(err)
	return response.Body
}

func safelyClose(closer io.ReadCloser) {
	if err := closer.Close(); err != nil {
		log.Fatal(err)
	}
}

func logIfPresent(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
