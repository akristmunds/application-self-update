package main

import (
	"bufio"
	"io"
	"log"
	"net/http"
	"os"
)

type httpResponseWriter struct {
	w http.ResponseWriter
}

func (w httpResponseWriter) write(contentType string, b []byte) {
	w.w.Header().Set("Content-Type", "text/plain")
	if _, err := w.w.Write(b); err != nil {
		log.Fatal(err)
	}
}

const latestVersion = "ver2"

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	})

	http.HandleFunc("/latestVersion", func(w http.ResponseWriter, r *http.Request) {
		log.Print("get latest version; latest version is:" + latestVersion)
		httpResponseWriter{w}.write("text/plain", []byte(latestVersion))
	})

	http.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
		log.Print("open file 'ver2'")
		file, fileError := os.Open("ver2")
		defer safelyClose(file)
		logIfPresent(fileError)
		stats, statsErr := file.Stat()
		logIfPresent(statsErr)
		log.Print("read file content")
		bytes := make([]byte, stats.Size())
		if _, err := bufio.NewReader(file).Read(bytes); err != nil {
			log.Fatal(err)
		}

		log.Print("write file content to response")
		httpResponseWriter{w}.write("application/binary", bytes)

	})

	log.Print("serving on 8081")
	log.Fatal(http.ListenAndServe(":8081", nil))

}

func safelyClose(closer io.ReadCloser) {
	if err := closer.Close(); err != nil {
		log.Fatal(err)
	}
}

func logIfPresent(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
